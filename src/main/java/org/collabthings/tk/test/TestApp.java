package org.collabthings.tk.test;

import org.collabthings.tk.CTButton;
import org.collabthings.tk.CTComposite;
import org.collabthings.tk.CTLabel;
import org.collabthings.tk.CTResourceManagerFactory;
import org.collabthings.tk.CTTabFolder;
import org.collabthings.tk.CTStyleAndResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class TestApp {
	private static Shell shell;

	private static void createContents() {
		shell = new Shell();

		GridLayout gl_shell = new GridLayout(1, false);
		gl_shell.verticalSpacing = SWT.FILL;
		gl_shell.horizontalSpacing = SWT.FILL;
		gl_shell.marginHeight = 1;
		gl_shell.marginWidth = 1;
		shell.setLayout(gl_shell);

		CTComposite upper = new CTComposite(shell, SWT.None);
		CTButton b = new CTButton(upper, SWT.None);
		b.setText("jee joo");

		CTComposite bottom = new CTComposite(shell, SWT.None);

		GridLayout gl_bottom = new GridLayout(1, false);
		gl_bottom.verticalSpacing = SWT.FILL;
		gl_bottom.horizontalSpacing = SWT.FILL;
		gl_bottom.marginHeight = 1;
		gl_bottom.marginWidth = 1;
		bottom.setLayout(gl_bottom);

		CTTabFolder tabs = new CTTabFolder(bottom, SWT.None);
		Composite ctab = tabs.getComposite();
		CTComposite testtab = new CTComposite(ctab, SWT.NONE);
		new CTLabel(testtab, SWT.NONE).setText("YEAH!!!");

		tabs.addTab("test", testtab, "test");
		tabs.addTab("test2", new CTComposite(ctab, SWT.NONE), "test2");
	}

	public static void main(String[] args) {

		Display display = Display.getDefault();

		CTResourceManagerFactory.setInstance(new CTStyleAndResources());

		createContents();
		//
		shell.open();
		shell.layout();
		shell.setMaximized(true);

		//
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

}
